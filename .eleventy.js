module.exports = function (config) {
  config.setLibrary("hbs", require("handlebars"));
  // Add a date formatter filter to Nunjucks
  config.addFilter("dateDisplay", require("./filters/dates.js"));
  config.addFilter("timestamp", require("./filters/timestamp.js"));
  config.addFilter("squash", require("./filters/squash.js"));

  // config.addShortcode("timelineEntry", function (type, entries) {
  //   var firstPart = ` <div class="card-group mx-2 mx-md-0 w-100">
  //   <svg class='timeline-icon text-secondary pull-left d-none d-md-block' xmlns="http://www.w3.org/2000/svg" width="8"
  //     height="8" viewBox="0 0 8 8">
  //     <path d="M0 0v8l4-4-4-4z" style="fill:#868E96;" transform="translate(2)" /></svg>
  //   <div class="card">
  //     <h6 class='card-header border-secondary font-italic text-capitalize'>${type}</h6>
  //     <ul class='list-group list-group-flush small'>`
  //   for (var i = 0; i < entries.length; i++) {
  //     firstPart += `<li class='list-group-item'>` + entries[i] + `</li>`
  //   }

  //   firstPart += `</ul>
  //   </div>
  // </div>`
  //   return firstPart;
  // });
  config.addPairedShortcode("timelineEntry", function (content, type, colour) {
    return `
    <div class="card">
      <h6 class='card-header border-${type} font-italic text-capitalize'>${type}</h6>
      <ul class='list-group list-group-flush'>
  ${content}
</ul>
</div>`
  });


  config.addPairedShortcode("timelineVersion", function (versionContent, versionCode, version, date) {
    return `<div class="timeline-separator">
    <h3 class='version text-capitalize' id='${versionCode}'>${version}
      <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" style="fill:#333333" viewBox="0 0 8 8">
        <path d="M1.5 0l-1.5 1.5 2.5 2.5-2.5 2.5 1.5 1.5 4-4-4-4z" transform="translate(1)" /></svg>
      <span class='text-muted'>${date}</span>
    </h3>
  </div>
  <div class='row'>
  <div class="card-group mx-2 mx-md-0 w-100">
    <svg class='timeline-icon text-secondary pull-left d-none d-md-block' xmlns="http://www.w3.org/2000/svg" width="8"
      height="8" viewBox="0 0 8 8">
      <path d="M0 0v8l4-4-4-4z"  transform="translate(2)"style="fill:#333333"></svg>
  ${versionContent}
</div>
</div>`
  });




  return {
    dir: {
      input: "src/site",
      output: "dist",
      includes: "_includes"
    },
    templateFormats: ["njk", "md", "hbs"],
    htmlTemplateEngine: "hbs",
    markdownTemplateEngine: "njk"
  };

};