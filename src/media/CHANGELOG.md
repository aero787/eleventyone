# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)

## [Unreleased]

## 1.5.2 - 2018-09-13 
### Added
- Mouse wheel support for rotary knobs

### Changed
- RUNWAY TURNOFF moved focus to illuminate taxiways on turns.
- ALTITUDE KNOB: Big knob now selects 100 or 1000 feet selection. Small knob changes altitude accordingly.
- New model for brake calipers.
- Eagle claw issue corrected.

### Removed
- A/P DISENGAGE BAR function has been removed for incorrect setting. (It will be added in the future)

## [1.5.1] - 2018-09-01 
### Fixed
- EXT PWR buttons are now set to off.
- STORM button is now set to off.
- Flaps indicator corrected.
- Corrections to center of gravity.
- Corrections to ground handling.
- Corrections to flight maneuverablity.
- Corrections to control surfaces.

## 1.5.0 - 2018-08-25
### Added
- Overhead 3D Buttons: Many still call one function (It will change in future updates)
- Overhead failure annunciators: Many still call one function (It will change in future updates)

### Changed
- New normal map texture (Old reflective properties removed from model)
- Fuselage nose roundness improved.
- Flaps detents added as -9 variant.
- Corrections to ground handling.
- Corrections to flight maneuverablity.

### Fixed
- CLOCK BUTTON (Now cycles between START, STOP, RESET)
- FLAP LEVER (Positions corrected for -9 variant)
- FUEL CONTROL LEVERS (Engine fire lights corrected)
- EXTINGUISHERS LEVERS (Now show engine fire annunciator)
- BOTTLE DISCHARGE ANNUNCIATORS
- Horizontal stabilizer trim invertion fixed.
- Corrected marching ants in belly and engines.
- Corrected issues with wingflex.
- Taxi light position corrected

