# The Magknight website
A friend has wanted me to use netlify for stuff for quite a while. So, the opertunity recently appeared to do just that. I spent a whole day fiddling with it, using a number of CMS, static site generators and other cool tech. I finally settled on eleventyone as my base, as it would allow me to use many different templating engines (for which i've selected handlebars primarily) and give a good demo for things that might be useful in the future.
Currently, it includes:
- Eleventy with our site config
- SASS compiling
(I scrapped all the other cool stuff eleventyone comes with, because I don't need it currently)

## Code used
- [EleventyOne](https://github.com/philhawksworth/eleventyone)
- [Bootstrap 4 timeline](https://github.com/ucffool/boostrap-4-timeline) - Used for the [changelog page](https://magknight.org/changelog)
- [Bootstrap (and "Product" Example](https://github.com/twbs/bootstrap)
