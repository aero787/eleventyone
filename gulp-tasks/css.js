var project = require('./_project.js');
var gulp = require('gulp');

gulp.task('css', function () {
	gulp.src(project.buildSrc + '/media/css/*')
		.pipe(gulp.dest(project.buildDest + '/media/css'));
		gulp.src(project.buildSrc + '/root/*')
		.pipe(gulp.dest(project.buildDest));
	return gulp.src(project.buildSrc + '/media/*')
		.pipe(gulp.dest(project.buildDest + '/media'));
});